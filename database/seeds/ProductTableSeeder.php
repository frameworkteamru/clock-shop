<?php

use Illuminate\Database\Seeder;


class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\Product\Product::class, 15)
            ->create()
            ->each(function ($u) {
                $u->productimages()->save(factory(App\Model\Product\ProductImage::class)->make());
        });
    }
}
