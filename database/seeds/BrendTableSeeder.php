<?php

use Illuminate\Database\Seeder;
use App\Model\Brend\Brend;

class BrendTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brend::create(['name' => 'Burberry']);
        Brend::create(['name' => 'Casio']);
        Brend::create(['name' => 'Escada']);
        Brend::create(['name' => 'Emporio Armani']);
        Brend::create(['name' => 'Jacques Lemans']);
    }
}
