<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Faker\Factory as Faker;


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Model\Product\Product::class, function () {

    $faker = Faker::create('ru_RU');

    return [
        'brend_id' => $faker->randomElement(App\Model\Brend\Brend::pluck('id')->toArray()),
        'name'     => 'Часы №'.$faker->numerify('###'),
        'price'    => $faker->numberBetween(1, 500)*100,
    ];
});
