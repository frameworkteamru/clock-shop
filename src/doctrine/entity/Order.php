<?php



/**
 * Order
 */
class Order
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var string
     */
    private $city_district;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $house_number;

    /**
     * @var string
     */
    private $entrance_number;

    /**
     * @var string
     */
    private $apartment_number;

    /**
     * @var integer
     */
    private $payment_method;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orderProducts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderProducts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Order
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Order
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Order
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set cityDistrict
     *
     * @param string $cityDistrict
     *
     * @return Order
     */
    public function setCityDistrict($cityDistrict)
    {
        $this->city_district = $cityDistrict;

        return $this;
    }

    /**
     * Get cityDistrict
     *
     * @return string
     */
    public function getCityDistrict()
    {
        return $this->city_district;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Order
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set houseNumber
     *
     * @param string $houseNumber
     *
     * @return Order
     */
    public function setHouseNumber($houseNumber)
    {
        $this->house_number = $houseNumber;

        return $this;
    }

    /**
     * Get houseNumber
     *
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->house_number;
    }

    /**
     * Set entranceNumber
     *
     * @param string $entranceNumber
     *
     * @return Order
     */
    public function setEntranceNumber($entranceNumber)
    {
        $this->entrance_number = $entranceNumber;

        return $this;
    }

    /**
     * Get entranceNumber
     *
     * @return string
     */
    public function getEntranceNumber()
    {
        return $this->entrance_number;
    }

    /**
     * Set apartmentNumber
     *
     * @param string $apartmentNumber
     *
     * @return Order
     */
    public function setApartmentNumber($apartmentNumber)
    {
        $this->apartment_number = $apartmentNumber;

        return $this;
    }

    /**
     * Get apartmentNumber
     *
     * @return string
     */
    public function getApartmentNumber()
    {
        return $this->apartment_number;
    }

    /**
     * Set paymentMethod
     *
     * @param integer $paymentMethod
     *
     * @return Order
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->payment_method = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return integer
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * Add orderProduct
     *
     * @param \OrderProduct $orderProduct
     *
     * @return Order
     */
    public function addOrderProduct(\OrderProduct $orderProduct)
    {
        $this->orderProducts[] = $orderProduct;

        return $this;
    }

    /**
     * Remove orderProduct
     *
     * @param \OrderProduct $orderProduct
     */
    public function removeOrderProduct(\OrderProduct $orderProduct)
    {
        $this->orderProducts->removeElement($orderProduct);
    }

    /**
     * Get orderProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }
    /**
     * @var integer
     */
    private $status;


    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var \User
     */
    private $user;


    /**
     * Set user
     *
     * @param \User $user
     *
     * @return Order
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User
     */
    public function getUser()
    {
        return $this->user;
    }
}
