<?php



/**
 * Product
 */
class Product
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orderProducts;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $shoppingCarts;

    /**
     * @var \ProductImage
     */
    private $productimage;

    /**
     * @var \Brend
     */
    private $brend;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orderProducts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->shoppingCarts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Product
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add orderProduct
     *
     * @param \OrderProduct $orderProduct
     *
     * @return Product
     */
    public function addOrderProduct(\OrderProduct $orderProduct)
    {
        $this->orderProducts[] = $orderProduct;

        return $this;
    }

    /**
     * Remove orderProduct
     *
     * @param \OrderProduct $orderProduct
     */
    public function removeOrderProduct(\OrderProduct $orderProduct)
    {
        $this->orderProducts->removeElement($orderProduct);
    }

    /**
     * Get orderProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrderProducts()
    {
        return $this->orderProducts;
    }

    /**
     * Add shoppingCart
     *
     * @param \ShoppingCart $shoppingCart
     *
     * @return Product
     */
    public function addShoppingCart(\ShoppingCart $shoppingCart)
    {
        $this->shoppingCarts[] = $shoppingCart;

        return $this;
    }

    /**
     * Remove shoppingCart
     *
     * @param \ShoppingCart $shoppingCart
     */
    public function removeShoppingCart(\ShoppingCart $shoppingCart)
    {
        $this->shoppingCarts->removeElement($shoppingCart);
    }

    /**
     * Get shoppingCarts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShoppingCarts()
    {
        return $this->shoppingCarts;
    }

    /**
     * Set productimage
     *
     * @param \ProductImage $productimage
     *
     * @return Product
     */
    public function setProductimage(\ProductImage $productimage = null)
    {
        $this->productimage = $productimage;

        return $this;
    }

    /**
     * Get productimage
     *
     * @return \ProductImage
     */
    public function getProductimage()
    {
        return $this->productimage;
    }

    /**
     * Set brend
     *
     * @param \Brend $brend
     *
     * @return Product
     */
    public function setBrend(\Brend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend
     *
     * @return \Brend
     */
    public function getBrend()
    {
        return $this->brend;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $productImages;


    /**
     * Add productImage
     *
     * @param \ProductImage $productImage
     *
     * @return Product
     */
    public function addProductImage(\ProductImage $productImage)
    {
        $this->productImages[] = $productImage;

        return $this;
    }

    /**
     * Remove productImage
     *
     * @param \ProductImage $productImage
     */
    public function removeProductImage(\ProductImage $productImage)
    {
        $this->productImages->removeElement($productImage);
    }

    /**
     * Get productImages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductImages()
    {
        return $this->productImages;
    }
}
