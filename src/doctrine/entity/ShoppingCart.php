<?php



/**
 * ShoppingCart
 */
class ShoppingCart
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $productAmount;

    /**
     * @var \Product
     */
    private $product;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ShoppingCart
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productAmount
     *
     * @param integer $productAmount
     *
     * @return ShoppingCart
     */
    public function setProductAmount($productAmount)
    {
        $this->productAmount = $productAmount;

        return $this;
    }

    /**
     * Get productAmount
     *
     * @return integer
     */
    public function getProductAmount()
    {
        return $this->productAmount;
    }

    /**
     * Set product
     *
     * @param \Product $product
     *
     * @return ShoppingCart
     */
    public function setProduct(\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Product
     */
    public function getProduct()
    {
        return $this->product;
    }
    /**
     * @var \User
     */
    private $user;


    /**
     * Set user
     *
     * @param \User $user
     *
     * @return ShoppingCart
     */
    public function setUser(\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \User
     */
    public function getUser()
    {
        return $this->user;
    }
}
