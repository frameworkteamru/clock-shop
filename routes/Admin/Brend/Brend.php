<?php
Route::group([
    'prefix' => 'brend',
    'as' => 'brend.',
    'namespace' => 'Brend',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'BrendController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'BrendController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'BrendController@save']);

    Route::group(['prefix' => "{brend}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'BrendController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'BrendController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'BrendController@delete']);

    });
    
});