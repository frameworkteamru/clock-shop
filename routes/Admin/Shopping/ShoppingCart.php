<?php
Route::group([
    'prefix' => 'cart',
    'as' => 'cart.',
    'namespace' => 'Shopping',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ShoppingCartController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ShoppingCartController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ShoppingCartController@save']);

    Route::group(['prefix' => "{shoppingCart}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'ShoppingCartController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'ShoppingCartController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'ShoppingCartController@delete']);

    });
    
});