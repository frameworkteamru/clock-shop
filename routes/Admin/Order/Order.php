<?php
Route::group([
    'prefix' => 'order',
    'as' => 'order.',
    'namespace' => 'Order',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'OrderController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'OrderController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'OrderController@save']);

    Route::group(['prefix' => "{order}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'OrderController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'OrderController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'OrderController@delete']);

        include __DIR__ . "/OrderProduct.php";
    });
    
});