<?php
Route::group([
    'prefix' => 'product',
    'as' => 'product.',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'OrderProductController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'OrderProductController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'OrderProductController@save']);

    Route::group(['prefix' => "{orderProduct}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'OrderProductController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'OrderProductController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'OrderProductController@delete']);

    });
    
});