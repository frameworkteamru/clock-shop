<?php
Route::group([
    'prefix' => 'product',
    'as' => 'product.',
    'namespace' => 'Product',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

    Route::group(['prefix' => "{product}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'ProductController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'ProductController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'ProductController@delete']);

    });
    
});