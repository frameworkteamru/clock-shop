<?php
Route::group([
    'prefix' => 'image',
    'as' => 'image.',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ProductImageController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductImageController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductImageController@save']);

    Route::group(['prefix' => "{productImage}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'ProductImageController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'ProductImageController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'ProductImageController@delete']);

    });
    
});