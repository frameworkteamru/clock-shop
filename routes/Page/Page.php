<?php
Route::group([
    'prefix' => 'page',
    'as' => 'page.',
    'namespace' => 'Page',
], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'PageController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'PageController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'PageController@save']);

    Route::group(['prefix' => "{page}"], function() {
        Route::get('edit',    ['as' => 'edit',   'uses' => 'PageController@edit']);
        Route::any('update',  ['as' => 'update', 'uses' => 'PageController@update']);
        Route::any('delete',  ['as' => 'delete', 'uses' => 'PageController@delete']);

    });
    
});