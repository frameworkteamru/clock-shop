<?php

    Route::group([
    'prefix' => 'brend',
    'as' => 'brend.',
    'namespace' => '\Brend',

    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'BrendController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'BrendController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'BrendController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'BrendController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'BrendController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'BrendController@delete']);

            });


    Route::group([
    'prefix' => 'order',
    'as' => 'order.',
    'namespace' => 'Order',

    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'OrderController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'OrderController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'OrderController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'OrderController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'OrderController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderController@delete']);

                                include 'Order/OrderProduct.php';
                        });


    Route::group([
    'prefix' => 'product',
    'as' => 'product.',
    'namespace' => '\Product',

    ], function () {

    Route::match(['get', 'post'], '', ['as' => 'index',  'uses' => 'ProductController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ProductController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductController@delete']);

                                include 'Product/ProductImage.php';
                        });


    Route::group([
    'prefix' => 'cart',
    'as' => 'cart.',
    'namespace' => '\ShoppingCart',

    ], function () {

    Route::get('',             ['as' => 'index',  'uses' => 'ShoppingCartController@index']);

    Route::get('create',       ['as' => 'create', 'uses' => 'ShoppingCartController@create']);
    Route::post('save',        ['as' => 'save',   'uses' => 'ShoppingCartController@save']);

    Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ShoppingCartController@edit']);
    Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ShoppingCartController@update']);

    Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ShoppingCartController@delete']);

            });

