<?php

Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    ], function () {
    
        Route::group([
        'prefix' => 'brend',
        'as' => 'brend.',
        'namespace' => 'Admin\Brend',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'BrendController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'BrendController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'BrendController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'BrendController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'BrendController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'BrendController@delete']);

                });

    
        Route::group([
        'prefix' => 'order',
        'as' => 'order.',
        'namespace' => '\Admin\Order',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'OrderController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'OrderController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'OrderController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'OrderController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'OrderController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'OrderController@delete']);

                                    include 'Admin/Order/OrderProduct.php';
                            });

    
        Route::group([
        'prefix' => 'product',
        'as' => 'product.',
        'namespace' => '\Admin\Product',

        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'ProductController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'ProductController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'ProductController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'ProductController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'ProductController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'ProductController@delete']);

                                    include 'Admin/Product/ProductImage.php';
                            });


        Route::group([
            'prefix' => 'page',
            'as' => 'page.',
            'namespace' => '\Admin\Page',
        ], function () {

        Route::get('',             ['as' => 'index',  'uses' => 'PageController@index']);

        Route::get('create',       ['as' => 'create', 'uses' => 'PageController@create']);
        Route::post('save',        ['as' => 'save',   'uses' => 'PageController@save']);

        Route::get('edit/{id}',    ['as' => 'edit',   'uses' => 'PageController@edit']);
        Route::any('update/{id}',  ['as' => 'update', 'uses' => 'PageController@update']);

        Route::any('delete/{id}',  ['as' => 'delete', 'uses' => 'PageController@delete']);

                });
        });
