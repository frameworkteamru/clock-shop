@extends('product.app')

@section('headers')
        <h1>
            Product
        </h1>
@endsection

@section('contents')
        <div class="row">
        @if($products->count())
            @foreach($products as $item)


                <div class="col-md-6" style="margin-bottom: 20px">
                    <div class="md-12">
                        <img src="{{ $item->productimages->first()->address }}"
                             class="img-responsive"
                             alt="{{ $item->productimages->first()->name }}">
                    </div>
                    <div class="col-md-6" style="margin-top: 5px">
                        <p>Модель: {{ $item->brend->name }}</p>
                        <p>Цена: {{ $item->price }} р.</p>
                    </div>
                    <div class="col-md-6" >
                        @if (Auth::guest())
                            <a href="{{ route('login') }}" class="btn btn-default btn-lg" role="button">В корзину</a>
                        @else
                            @include('product.modalForm')
                        @endif

                    </div>
                </div>








            @endforeach
            {!! $products->render() !!}
        @else
            <h3 class="text-center alert alert-info">Empty!</h3>
        @endif
    </div>
@endsection