@extends('layouts.modal')

@section('nameButton')
    В корзину
@endsection

@section('name')
    <h4 class="modal-title">Введите колличество:</h4>
@endsection

@section('form')
    <form method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" name="cart[product_amount]" value="1">
        <input type="hidden" name="cart[user_id]" value=" {{ Auth::id() }} ">
        <input type="hidden" name="cart[product_id]" value="{{ $item->id }}">

        <button type="submit" class="btn btn-primary">Send</button>
        {{ dump( Auth::id() ) }}
    </form>
@endsection

