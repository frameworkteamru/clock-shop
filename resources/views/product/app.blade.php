@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading" >
                    <h1>
                        @yield('headers')
                    </h1>
                </div>
                <div class="panel-body">
                    @yield('contents')
                </div>
            </div>
        </div>
    </div>
@endsection