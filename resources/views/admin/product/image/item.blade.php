<tr>
    <td>{{ $item->id }}</td>
    <td class="text-right">
        <a class="btn btn-xs btn-warning" href="{{ route('\admin.product.image.edit', $item->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
        <form action="{{ route('\admin.product.image.delete', $item->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
        </form>
    </td>
</tr>