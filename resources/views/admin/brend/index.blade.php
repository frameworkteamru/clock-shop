@extends('layouts.app')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Brend
            <a class="btn btn-success pull-right" href="{{ route('admin.brend.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('.admin.brend.filter')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if($brends->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($brends as $item)
                            @include('admin.brend.item')
                        @endforeach
                    </tbody>
                </table>
                {!! $brends->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif
        </div>
    </div>
@endsection