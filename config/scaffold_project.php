<?php

return [

// Префиксы
    'prefix' => [
        '' => [
            'Admin',
            ''
        ]
    ],

    // Основные сущности
    'masters' => [
        'Brend',
        'Order',
        'Product',
        'ShoppingCart',
    ],

    // Подчененные сущности
    'subordinates' => [

        'Order' => [
            'OrderProduct',
        ],
        'Product' => [
            'ProductImage',
        ],
    ],

    'options' => [
        '--force' => true,
    ],

];