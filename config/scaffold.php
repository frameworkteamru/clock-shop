<?php

return [

    /* Путь к шаблонам генерации. По умолчанию они лежат в vendor/dom1no/scaffold/src/stubs/blade as blade
     * Чтобы подключить свои шаблоны подключите их в app/Providers/RouteServiceProvider@boot:
     * app('view')->addNamespace('name', base_path('path/to/stubs'));
    */
    'stubPath' => 'blade::', //name

    /*
     * Path for scaffolding controllers.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'controllers_path' => '../app/Http/Controllers/',

    /*
     * Path for scaffolding base models.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'bases_path' => '../app/Model/Base/',

    /*
     * Path for scaffolding models.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'models_path' => '../app/Model/',

    /*
     * Path for scaffolding views.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'views_path' => '../resources/views/',

    /*
     * Path for scaffolding migration.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'migrations_path' => '../database/migrations/',

    /*
     * Path for scaffolding migration.
     * If 'prefix' is used when scaffolding
     * that folder will be added to these paths
    */
    'routes_path' => '../routes/',

    /*
     * Список генерируемых view
    */
    'views' => ['index', 'edit', 'create', 'item', 'filter'],
    /*
     * Путь до doctrine/yaml
     */
    'metadata_path' => '/database/doctrine/yaml',

    /*
    * include в route неподчененных сущностей
     */
    'includes' =>
        [
            /*
             * *** Пример ***
        'Product' =>
            [
            'User',
            'UserRole',
            ],
            */
        ],
];
