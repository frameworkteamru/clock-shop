#!/bin/bash
clear

ENTITYDIR=src/doctrine/entity/

if [ ! -d "$ENTITYDIR" ]; then
	echo "Creating Entities Directory"
	mkdir $ENTITYDIR
fi

php doctrine orm:generate:entities --no-backup $ENTITYDIR
php doctrine orm:validate-schema

echo "Update database schema? (y/n)"
read answer

if [ "$answer" == "y" ]; then
  php doctrine orm:schema-tool:update --force --dump-sql
fi

clear
