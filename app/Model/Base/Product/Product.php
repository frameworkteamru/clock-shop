<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $table = 'product';

    protected $fillable = [
        'brend_id',
        'name',
        'price',
    ];

    public function orderproducts()
    {
        return $this->hasMany('App\Model\Order\OrderProduct', 'product_id', 'id');
    }

    public function shoppingcarts()
    {
        return $this->hasMany('App\Model\Shopping\ShoppingCart', 'product_id', 'id');
    }

    public function productimages()
    {
        return $this->hasMany('App\Model\Product\ProductImage', 'product_id', 'id');
    }

    public function brend()
    {
        return $this->belongsTo('App\Model\Brend\Brend', 'brend_id', 'id');
    }
}