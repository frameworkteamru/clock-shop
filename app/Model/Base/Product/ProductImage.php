<?php
namespace App\Model\Base\Product;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    public $timestamps = false;

    protected $table = 'product__image';

    protected $fillable = [
        'product_id',
        'name',
        'address',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}