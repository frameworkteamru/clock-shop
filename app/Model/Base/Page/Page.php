<?php
namespace App\Model\Base\Page;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public $timestamps = true;

    protected $table = 'page';

    protected $fillable = [
        'name',
        'text',
        'updated_at',
    ];
}