<?php
namespace App\Model\Base\Brend;

use Illuminate\Database\Eloquent\Model;

class Brend extends Model
{
    public $timestamps = false;

    protected $table = 'brend';

    protected $fillable = [
        'name',
    ];

    public function products()
    {
        return $this->hasMany('App\Model\Product\Product', 'brend_id', 'id');
    }
}