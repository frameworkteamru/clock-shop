<?php
namespace App\Model\Base\Order;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    public $timestamps = false;

    protected $table = 'order__product';

    protected $fillable = [
        'order_id',
        'product_id',
        'amount',
        'price',
    ];

    public function order()
    {
        return $this->belongsTo('App\Model\Order\Order', 'order_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }
}