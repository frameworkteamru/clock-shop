<?php
namespace App\Model\Base\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;

    protected $table = 'order';

    protected $fillable = [
        'user_id',
        'date',
        'price',
        'city_district',
        'street',
        'house_number',
        'entrance_number',
        'apartment_number',
        'payment_method',
        'status',
    ];

    public function orderproducts()
    {
        return $this->hasMany('App\Model\Order\OrderProduct', 'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User\User', 'user_id', 'id');
    }
}