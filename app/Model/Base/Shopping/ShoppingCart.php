<?php
namespace App\Model\Base\Shopping;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    public $timestamps = false;

    protected $table = 'shopping__cart';

    protected $fillable = [
        'product_id',
        'user_id',
        'product_amount',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product\Product', 'product_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User\User', 'user_id', 'id');
    }
}