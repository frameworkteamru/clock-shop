<?php
namespace App\Model\Order;

use App\Model\Base\Order\OrderProduct as BaseOrderProduct;

class OrderProduct extends BaseOrderProduct
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addOrderProductFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = OrderProduct::noFilter($query);
        $query = OrderProduct::addOrderProductFilter($query, $params);

        return $query;
    }
}