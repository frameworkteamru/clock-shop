<?php
namespace App\Model\Order;

use App\Model\Base\Order\Order as BaseOrder;

class Order extends BaseOrder
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addOrderFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Order::noFilter($query);
        $query = Order::addOrderFilter($query, $params);

        return $query;
    }
}