<?php
namespace App\Model\Shopping;

use App\Model\Base\Shopping\ShoppingCart as BaseShoppingCart;
use Illuminate\Http\Request;

class ShoppingCart extends BaseShoppingCart
{
    static $listFields = [];

    static $rules = [];

    public static function addCart(Request $request)
    {
        if ($request->input('cart')) {
            $cartId = ShoppingCart::checkProductAmount($request->input('cart'));

            // Если после выборки, элемент не получен, будет значение null
            if(is_null($cartId)) {
                $shoppingCart = new ShoppingCart($request->input('cart'));
                $shoppingCart ->save();
            } else {
                $shoppingCart = ShoppingCart::find($cartId);
                $old = $request->input('cart')['product_amount'];
                $shoppingCart->update(['product_amount' => $shoppingCart->product_amount + $old ]);
            }
        }
    }

    /***
     * Обязательно должны быть user_id и product_id
     * @param $params
     * @return mixed
     *
     * Выводит значение id корзины, в которой есть данные параметры
     */
    public static function getId ($params)
    {
        $shoppingCartUser = ShoppingCart::
            where(
                'user_id',
                '=',
                $params['user_id'])
            ->where(
                'product_id',
                '=',
                $params['product_id']
            )->value('id');

        return $shoppingCartUser;
    }

}