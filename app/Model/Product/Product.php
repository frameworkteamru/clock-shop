<?php
namespace App\Model\Product;

use App\Model\Base\Product\Product as BaseProduct;

class Product extends BaseProduct
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addProductFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Product::noFilter($query);
        $query = Product::addProductFilter($query, $params);

        return $query;
    }
}