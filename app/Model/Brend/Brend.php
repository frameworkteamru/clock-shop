<?php
namespace App\Model\Brend;

use App\Model\Base\Brend\Brend as BaseBrend;

class Brend extends BaseBrend
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addBrendFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Brend::noFilter($query);
        $query = Brend::addBrendFilter($query, $params);

        return $query;
    }
}