<?php
namespace App\Model\Page;

use App\Model\Base\Page\Page as BasePage;

class Page extends BasePage
{
    static $listFields = [];

    static $rules = [];

    public function scopeNoFilter($query)
    {
        return $query;
    }

    public function addPageFilter($query, $params)
    {
        /* EXAMPLE
        *    if (isset($params['name']) && !empty($params['name'])) {
        *       $query->where('name', 'LIKE', '%' . $params['name'] . '%');
        *    }
        */

        return $query;
    }

    public function scopeFilter($query, $params)
    {
        $query = Page::noFilter($query);
        $query = Page::addPageFilter($query, $params);

        return $query;
    }
}