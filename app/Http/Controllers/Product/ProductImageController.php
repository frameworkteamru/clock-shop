<?php
namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductImage;
use Illuminate\Http\Request;



class ProductImageController extends Controller
{

    public function index()
    {
        $productImages = ProductImage::orderBy('id', 'desc')->paginate(10);

        return view('product.image.index', [
            'productImages' => $productImages
        ]);
    }

    public function create()
    {
        return view('product.image.create');
    }

    public function save(Request $request)
    {
        $productImage = new ProductImage($request->all());

        $productImage->save();

        return redirect()->route('product.image.index');
    }

    public function edit(ProductImage $productImage)
    {
        return view('product.image.edit', [
            'productImage' => $productImage
        ]);
    }

    public function update(Request $request, ProductImage $productImage)
    {
        $productImage->update($request->all());

        return redirect()->route('product.image.index');
    }

    public function delete(ProductImage $productImage)
    {
        $productImage->delete();

        return redirect()->back();
    }
}