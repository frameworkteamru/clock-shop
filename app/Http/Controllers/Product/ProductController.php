<?php
namespace App\Http\Controllers\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\Product;
use App\Model\Shopping\ShoppingCart;
use Illuminate\Http\Request;



class ProductController extends Controller
{

    public function index(Request $request)
    {
        ShoppingCart::addCart($request);

        $products = Product::with(['productimages', 'brend'])->orderBy('id', 'desc')->paginate(10);

        return view('product.index', [
            'products' => $products
        ]);
    }

    public function create()
    {
        return view('product.create');
    }

    public function save(Request $request)
    {
        $product = new Product($request->all());

        $product->save();

        return redirect()->route('product.index');
    }

    public function edit(Product $product)
    {
        return view('product.edit', [
            'product' => $product
        ]);
    }

    public function update(Request $request, Product $product)
    {
        $product->update($request->all());

        return redirect()->route('product.index');
    }

    public function delete(Product $product)
    {
        $product->delete();

        return redirect()->back();
    }
}