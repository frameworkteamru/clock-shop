<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\Product;
use Illuminate\Http\Request;



class ProductController extends Controller
{

    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.index', [
            'products' => $products
        ]);
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function save(Request $request)
    {
        $product = new Product($request->all());

        $product->save();

        return redirect()->route('admin.product.index');
    }

    public function edit(Product $product)
    {
        return view('admin.product.edit', [
            'product' => $product
        ]);
    }

    public function update(Request $request, Product $product)
    {
        $product->update($request->all());

        return redirect()->route('admin.product.index');
    }

    public function delete(Product $product)
    {
        $product->delete();

        return redirect()->back();
    }
}