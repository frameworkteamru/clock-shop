<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Product\ProductImage;
use Illuminate\Http\Request;



class ProductImageController extends Controller
{

    public function index()
    {
        $images = ProductImage::orderBy('id', 'desc')->paginate(10);

        return view('admin.product.image.index', [
            'images' => $images
        ]);
    }

    public function create()
    {
        return view('admin.product.image.create');
    }

    public function save(Request $request)
    {
        $productImage = new ProductImage($request->all());

        $productImage->save();

        return redirect()->route('admin.product.image.index');
    }

    public function edit(ProductImage $productImage)
    {
        return view('admin.product.image.edit', [
            'productImage' => $productImage
        ]);
    }

    public function update(Request $request, ProductImage $productImage)
    {
        $productImage->update($request->all());

        return redirect()->route('admin.product.image.index');
    }

    public function delete(ProductImage $productImage)
    {
        $productImage->delete();

        return redirect()->back();
    }
}