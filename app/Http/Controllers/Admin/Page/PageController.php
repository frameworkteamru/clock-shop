<?php
namespace App\Http\Controllers\Admin\Page;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Page\Page;
use Illuminate\Http\Request;



class PageController extends Controller
{

    public function index()
    {
        $pages = Page::orderBy('id', 'desc')->paginate(10);

        return view('admin.page.index', [
            'pages' => $pages
        ]);
    }

    public function create()
    {
        return view('admin.page.create');
    }

    public function save(Request $request)
    {
        $page = new Page($request->all());

        $page->save();

        return redirect()->route('admin.page.index');
    }

    public function edit(Page $page)
    {
        return view('admin.page.edit', [
            'page' => $page
        ]);
    }

    public function update(Request $request, Page $page)
    {
        $page->update($request->all());

        return redirect()->route('admin.page.index');
    }

    public function delete(Page $page)
    {
        $page->delete();

        return redirect()->back();
    }
}