<?php
namespace App\Http\Controllers\Admin\Brend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Brend\Brend;
use Illuminate\Http\Request;



class BrendController extends Controller
{

    public function index()
    {
        $brends = Brend::orderBy('id', 'desc')->paginate(10);

        return view('admin.brend.index', [
            'brends' => $brends
        ]);
    }

    public function create()
    {
        return view('admin.brend.create');
    }

    public function save(Request $request)
    {
        $brend = new Brend($request->all());

        $brend->save();

        return redirect()->route('admin.brend.index');
    }

    public function edit(Brend $brend)
    {
        return view('admin.brend.edit', [
            'brend' => $brend
        ]);
    }

    public function update(Request $request, Brend $brend)
    {
        $brend->update($request->all());

        return redirect()->route('admin.brend.index');
    }

    public function delete(Brend $brend)
    {
        $brend->delete();

        return redirect()->back();
    }
}