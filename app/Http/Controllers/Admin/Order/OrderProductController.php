<?php
namespace App\Http\Controllers\Admin\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Order\OrderProduct;
use Illuminate\Http\Request;

use App\Model\Order\Order;


class OrderProductController extends Controller
{

    public function index(Order $order)
    {
        $orderProducts = $order->orderProducts()->orderBy('id', 'desc')->paginate(10);

        return view('admin.order.product.index', [
            'orderProducts' => $orderProducts,
            'order' => $order
        ]);
    }

    public function create(Order $order)
    {
        return view('admin.order.product.create', [
            'order' => $order
        ]);
    }

    public function save(Request $request, Order $order)
    {
        $orderProduct = new OrderProduct($request->all());

        $orderProduct->save();

        return redirect()->route('admin.order.product.index', $order);
    }

    public function edit(Order $order, OrderProduct $orderProduct)
    {
        return view('admin.order.product.edit', [
            'orderProduct' => $orderProduct,
            'order' => $order
        ]);
    }

    public function update(Request $request, Order $order, OrderProduct $orderProduct)
    {
        $orderProduct->update($request->all());

        return redirect()->route('admin.order.product.index', $order);
    }

    public function delete(Order $order, OrderProduct $orderProduct)
    {
        $orderProduct->delete();

        return redirect()->back();
    }
}