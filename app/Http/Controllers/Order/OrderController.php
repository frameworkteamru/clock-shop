<?php
namespace App\Http\Controllers\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Order\Order;
use Illuminate\Http\Request;



class OrderController extends Controller
{

    public function index()
    {
        $orders = Order::orderBy('id', 'desc')->paginate(10);

        return view('order.index', [
            'orders' => $orders
        ]);
    }

    public function create()
    {
        return view('order.create');
    }

    public function save(Request $request)
    {
        $order = new Order($request->all());

        $order->save();

        return redirect()->route('order.index');
    }

    public function edit(Order $order)
    {
        return view('order.edit', [
            'order' => $order
        ]);
    }

    public function update(Request $request, Order $order)
    {
        $order->update($request->all());

        return redirect()->route('order.index');
    }

    public function delete(Order $order)
    {
        $order->delete();

        return redirect()->back();
    }
}