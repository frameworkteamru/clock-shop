<?php
namespace App\Http\Controllers\ShoppingCart;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\Shopping\ShoppingCart;
use Illuminate\Http\Request;



class ShoppingCartController extends Controller
{

    public function index()
    {
        $shoppingCarts = ShoppingCart::orderBy('id', 'desc')->paginate(10);

        return view('shopping.cart.index', [
            'shoppingCarts' => $shoppingCarts
        ]);
    }

    public function create()
    {
        return view('shopping.cart.create');
    }

    public function save(Request $request)
    {
        $shoppingCart = new ShoppingCart($request->all());

        $shoppingCart->save();

        return redirect()->route('shopping.cart.index');
    }

    public function edit(ShoppingCart $shoppingCart)
    {
        return view('shopping.cart.edit', [
            'shoppingCart' => $shoppingCart
        ]);
    }

    public function update(Request $request, ShoppingCart $shoppingCart)
    {
        $shoppingCart->update($request->all());

        return redirect()->route('shopping.cart.index');
    }

    public function delete(ShoppingCart $shoppingCart)
    {
        $shoppingCart->delete();

        return redirect()->back();
    }
}